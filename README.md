# Desafio Ioasys
Para mais informações sobre o objetivo do projeto, [acessar repositório da Ioasys]( https://bitbucket.org/ioasys/empresas-dotnet/src/master/)

## Instruções para execução
É necessário ter o MySQL rodando e lembrar de configurar a connection string para tal. 
Tem uma collection do Postman com alguns requests de exemplo. 

## Possiveis futuras melhorias
* Usar Guid no lugar de inteiro para o Id das entitades. Foi escolhido usar inteiro pela praticidade de se fazer as requisições no Postman
* O campo Role na classe User poderia ser um Array, criando um cenário em que o usuário poderia ter mais de uma role.
* Poderia ter sido usado Identity para lidar com a parte de Login.
* Outra sugestão seria separar a parte autenticação em um microsservico, caso essa fosse a escolha de arquitetura. Isso permitiria que essa parte ficasse acessivel a outros microsservicos sem aumenetar o acoplamento ao microsservico atual.
