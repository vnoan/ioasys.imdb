﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Utils
{
    public class PagedList<T>
    {
        public IList<T> Page { get; }

        public int Count { get; }
        public int PageCount { get; }
        public int PageSize { get; }
        public int CurrentPage { get; }

        public PagedList()
        {

        }

        public PagedList(IList<T> data, int count, int pageSize, int currentPage)
        {
            if (pageSize < 0)
                throw new ArgumentOutOfRangeException(nameof(pageSize));

            Count = count;
            PageSize = pageSize;
            Page = data;
            CurrentPage = currentPage;
            PageCount = Convert.ToInt32(Math.Ceiling(Count / (double)PageSize));
        }
    }
}
