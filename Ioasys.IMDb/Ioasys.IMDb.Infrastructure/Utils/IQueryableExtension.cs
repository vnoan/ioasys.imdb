﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Utils
{
    public static class IQueryableExtensions
    {
        public static async Task<PagedList<T>> ToPagedList<T>(this IQueryable<T> query, int page, int pageSize)
        {
            var count = await query.CountAsync();

            if (page <= 0) page = 1;

            var result = await query
                .Skip(pageSize * (page - 1))
                .Take(pageSize)
                .ToListAsync();

            return new PagedList<T>(result, count, pageSize, page);
        }
    }
}
