﻿using Ioasys.IMDb.Domain.Aggregates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;

namespace Ioasys.IMDb.Infrastructure.Mappings
{
    public class MovieMapConfig
    {
        internal static Action<EntityTypeBuilder<Movie>> ConfigureMap()
        {
            return entity =>
            {
                entity.ToTable("Movies");
                entity.HasKey(x => x.Id);
                entity.HasQueryFilter(x => x.DeletedAt == null);

                entity.Property(x => x.Name).HasMaxLength(200);

                entity.HasData(Seed());
            };
        }

        private static List<Movie> Seed()
        {
            return new List<Movie>
            {
                new Movie(1, "Star Wars"),
                new Movie(2, "Harry Potter"),
                new Movie(3, "Vingadores"),
            };
        }
    }
}
