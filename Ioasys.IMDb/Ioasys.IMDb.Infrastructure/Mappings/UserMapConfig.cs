﻿using Ioasys.IMDb.Domain.Aggregates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;

namespace Ioasys.IMDb.Infrastructure.Mappings
{
    public class UserMapConfig
    {
        internal static Action<EntityTypeBuilder<User>> ConfigureMap()
        {
            return entity =>
            {
                entity.ToTable("Users");
                entity.HasKey(x => x.Id);
                entity.HasQueryFilter(x => x.DeletedAt == null);

                entity.Property(x => x.Password);
                entity.Property(x => x.Username).HasMaxLength(200);

                entity.HasData(Seed());
            };
        }

        private static List<User> Seed()
        {
            return new List<User>
            {
                new User(1, "admin", "25f43b1486ad95a1398e3eeb3d83bc4010015fcc9bedb35b432e00298d5021f7", "admin"),
                new User(2, "user1", "0a041b9462caa4a31bac3567e0b6e6fd9100787db2ab433d96f6d178cabfce90"),
                new User(3, "user2", "6025d18fe48abd45168528f18a82e265dd98d421a7084aa09f61b341703901a3"),
                new User(4, "user3", "5860faf02b6bc6222ba5aca523560f0e364ccd8b67bee486fe8bf7c01d492ccb")
            };

        }
    }
}
