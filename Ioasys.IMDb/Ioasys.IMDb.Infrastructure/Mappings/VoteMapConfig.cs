﻿using Ioasys.IMDb.Domain.Aggregates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Ioasys.IMDb.Infrastructure.Mappings
{
    public class VoteMapConfig
    {
        internal static Action<EntityTypeBuilder<Vote>> ConfigureMap()
        {
            return entity =>
            {
                entity.ToTable("Votes");
                entity.HasKey(x => x.Id);
                entity.HasQueryFilter(x => x.DeletedAt == null);

                entity.HasOne(x => x.Movie).WithMany(x => x.Votes).HasForeignKey(x => x.MovieId);
                entity.HasOne(x => x.User).WithMany(x => x.Votes).HasForeignKey(x => x.UserId);
            };
        }
    }
}
