﻿using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Domain.Utils;
using Ioasys.IMDb.Infrastructure.Mappings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure
{
    public class DomainDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<User> Users { get; set; }

        public DomainDbContext(DbContextOptions<DomainDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity(MovieMapConfig.ConfigureMap());
            modelBuilder.Entity(UserMapConfig.ConfigureMap());
            modelBuilder.Entity(VoteMapConfig.ConfigureMap());
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var changeSet = ChangeTracker.Entries<Entity>().ToList();
            foreach (var entityEntry in changeSet)
            {
                switch (entityEntry.State)
                {
                    case EntityState.Modified:
                        entityEntry.Entity.UpdatedAt = DateTimeOffset.UtcNow;
                        break;
                    case EntityState.Added:
                        entityEntry.Entity.CreatedAt = DateTimeOffset.UtcNow;
                        break;
                    case EntityState.Deleted:
                        entityEntry.Entity.DeletedAt = DateTimeOffset.UtcNow;
                        entityEntry.State = EntityState.Modified;
                        break;
                    default:
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges()
        {
            throw new Exception("Use SaveChangesAsync");
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            throw new Exception("Use SaveChangesAsync");
        }
    }
}
