﻿using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Infrastructure.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Repositories.MovieRepository
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(DomainDbContext context) : base(context)
        {
        }

        public async Task<Movie> Get(string name, CancellationToken cancellationToken = default)
        {
            return await _context.Movies.Where(x => x.Name == name).FirstOrDefaultAsync(cancellationToken);
        }

        public async Task<PagedList<Movie>> GetAllPaged(string nameFilter, int pageSize, int pageNumber)
        {
            return await _context.Movies
                .Where(x => string.IsNullOrEmpty(nameFilter) || x.Name.ToLower().Contains(nameFilter.ToLower().Trim()))
                .ToPagedList(pageNumber, pageSize);
        }
    }
}
