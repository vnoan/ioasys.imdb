﻿using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Infrastructure.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Repositories.MovieRepository
{
    public interface IMovieRepository : IRepository<Movie>
    {
        Task<PagedList<Movie>> GetAllPaged(string nameFilter, int pageSize, int pageNumber);
        Task<Movie> Get(string name, CancellationToken cancellationToken = default);
    }
}
