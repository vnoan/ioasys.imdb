﻿using Ioasys.IMDb.Domain.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Repositories
{

        public abstract class Repository<TEntity> : IRepository<TEntity>
            where TEntity : Entity
        {
            protected readonly DomainDbContext _context;
            public Repository(DomainDbContext context)
            {
                _context = context;
            }

            public async Task<TEntity> Add(TEntity obj, CancellationToken cancellationToken = default)
            {
                var entity = await _context.Set<TEntity>().AddAsync(obj);
                await _context.SaveChangesAsync(cancellationToken);
                return entity.Entity;
            }

            public async Task AddRange(List<TEntity> objs, CancellationToken cancellationToken = default)
            {
                await _context.Set<TEntity>().AddRangeAsync(objs);
                await _context.SaveChangesAsync(cancellationToken);
            }

            public async Task<TEntity> Get(int id, CancellationToken cancellationToken = default)
            {
                var data = await _context.Set<TEntity>()
                    .Where(x => x.Id == id)
                    .FirstOrDefaultAsync();
                return data;
            }

            public async Task<List<TEntity>> GetAll(CancellationToken cancellationToken = default)
            {
                var data = await _context.Set<TEntity>().ToListAsync();
                return data;
            }

            public virtual async Task<TEntity> Update(TEntity obj, CancellationToken cancellationToken = default)
            {
                var data = _context.Set<TEntity>().Update(obj);
                await _context.SaveChangesAsync(cancellationToken);
                return data.Entity;
            }

            public async Task UpdateRange(List<TEntity> objs, CancellationToken cancellationToken = default)
            {
                _context.Set<TEntity>().UpdateRange(objs);
                await _context.SaveChangesAsync(cancellationToken);
            }

            public async Task Delete(TEntity obj, CancellationToken cancellationToken = default)
            {
                _context.Set<TEntity>().Remove(obj);
                await _context.SaveChangesAsync(cancellationToken);
            }

            public async Task DeleteRange(List<TEntity> objs, CancellationToken cancellationToken = default)
            {
                _context.Set<TEntity>().RemoveRange(objs);
                await _context.SaveChangesAsync(cancellationToken);
            }
        }

}
