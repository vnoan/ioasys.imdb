﻿using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Infrastructure.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Repositories.VoteRepository
{
    public interface IVoteRepository : IRepository<Vote>
    {
        Task<Vote> GetByUserAndMovie(int userId, int movieId, CancellationToken cancellationToken = default);
    }
}
