﻿using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Infrastructure.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Repositories.VoteRepository
{
    public class VoteRepository : Repository<Vote>, IVoteRepository
    {
        public VoteRepository(DomainDbContext context) : base(context)
        {
        }

        public async Task<Vote> GetByUserAndMovie(int userId, int movieId, CancellationToken cancellationToken = default)
        {
            return await _context.Votes
                .AsSplitQuery()
                .Where(x => x.UserId == userId && x.MovieId == movieId)
                .Include(x => x.Movie)
                .Include(x => x.User)
                .FirstOrDefaultAsync(cancellationToken);
        }

    }
}
