﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Repositories
{
    public interface IRepository<TEntity>
    {
        public Task<TEntity> Get(int id, CancellationToken cancellationToken = default);
        public Task<TEntity> Add(TEntity obj, CancellationToken cancellationToken = default);
        public Task AddRange(List<TEntity> obj, CancellationToken cancellationToken = default);
        public Task<TEntity> Update(TEntity obj, CancellationToken cancellationToken = default);
        public Task UpdateRange(List<TEntity> obj, CancellationToken cancellationToken = default);
        public Task Delete(TEntity obj, CancellationToken cancellationToken = default);
        public Task<List<TEntity>> GetAll(CancellationToken cancellationToken = default);
        public Task DeleteRange(List<TEntity> objs, CancellationToken cancellationToken = default);
    }
}
