﻿using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Infrastructure.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Repositories.MovieRepository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DomainDbContext context) : base(context)
        {

        }

        public async Task<User> Get(string username, CancellationToken cancellationToken = default)
        {
            return await _context.Users
                .Where(x => x.Username == username)
                .FirstOrDefaultAsync(cancellationToken);
        }
    }
}
