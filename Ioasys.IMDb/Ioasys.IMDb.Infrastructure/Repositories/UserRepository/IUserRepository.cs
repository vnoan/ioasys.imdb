﻿using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Infrastructure.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Infrastructure.Repositories.MovieRepository
{
    public interface IUserRepository : IRepository<User>
    {
        public Task<User> Get(string username, CancellationToken cancellationToken = default);
    }
}
