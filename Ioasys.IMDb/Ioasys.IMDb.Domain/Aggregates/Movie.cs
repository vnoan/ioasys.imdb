﻿using Ioasys.IMDb.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Domain.Aggregates
{
    public class Movie : Entity
    {
        public string Name { get; set; }
        public decimal Rate { get; private set; }
        public int CountVotes { get; private set; }

        public List<Vote> Votes { get; set; } = new List<Vote>();

        public Movie(string name)
        {
            Name = name;
            Rate = 0;
            CountVotes = 0;
        }

        public Movie(int id, string name)
        {
            Id = id;
            Name = name;
            Rate = 0;
            CountVotes = 0;
        }

        public void AddVote(Vote vote)
        {
            Votes.Add(vote);
            Rate = ((Rate * CountVotes) + vote.Rate) / (CountVotes + 1);
            CountVotes += 1;
        }
        public void RemoveVote(Vote vote)
        {
            var removeResult = Votes.Remove(vote);
            if (!removeResult)
                throw new ArgumentException("Vote not found", nameof(vote));

            var totalRate = Rate * CountVotes;
            var adjustedRate = totalRate - vote.Rate;
            CountVotes -= 1;
            Rate = adjustedRate * CountVotes;
        }

    }
}
