﻿using Ioasys.IMDb.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Domain.Aggregates
{
    public class User : Entity
    {
        public string Username { get; private set; }
        public string Password { get; private set; }
        public string Role { get; private set; }

        public List<Vote> Votes { get; private set; }

        public User(string username, string password, string role = "user")
        {
            Username = username;
            Password = password;
            Role = role;
        }

        public User(int id, string username, string password, string role = "user")
        {
            Id = id;
            Username = username;
            Password = password;
            Role = role;
        }


        public void SetAdmin()
        {
            Role = "admin";
        }

        public void UpdateUser(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public void UpdateUser(string username, string password, bool isAdmin)
        {
            Username = username;
            Password = password;
            Role = isAdmin ? "admin" : "user";
        }

    }
}
