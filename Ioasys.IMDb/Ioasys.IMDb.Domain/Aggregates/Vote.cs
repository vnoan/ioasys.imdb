﻿using Ioasys.IMDb.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Domain.Aggregates
{
    public class Vote : Entity
    {
        public int UserId { get; private set; }
        public User User { get; private set; }
        public int MovieId { get; private set; }
        public Movie Movie { get; private set; }
        public decimal Rate { get; private set; }

        public Vote(int userId, int movieId, decimal rate)
        {
            if(rate < 0 || rate > 4)
            {
                throw new ArgumentOutOfRangeException("rate", "Rate bigger than 4 or smaller than 0");
            }

            UserId = userId;
            MovieId = movieId;
            Rate = rate;
        }

        public void UpdateRate(decimal rate)
        {
            if (rate < 0 || rate > 4)
            {
                throw new ArgumentOutOfRangeException("rate", "Rate bigger than 4 or smaller than 0");
            }

            Rate = rate;
        }
    }
}
