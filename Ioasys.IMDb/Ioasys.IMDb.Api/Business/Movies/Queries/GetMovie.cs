﻿using Ioasys.IMDb.Api.Business.Movies.DTOs;
using Ioasys.IMDb.Infrastructure.Repositories.MovieRepository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Api.Business.Movies.Queries
{
    public class GetMovie
    {
        public class Contract : IRequest<IActionResult>
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<Contract, IActionResult>
        {
            private readonly IMovieRepository movieRepository;

            public Handler(IMovieRepository movieRepository)
            {
                this.movieRepository = movieRepository;
            }

            public async Task<IActionResult> Handle(Contract request, CancellationToken cancellationToken)
            {
                var dbMovie = await movieRepository.Get(request.Id, cancellationToken);

                if (dbMovie == null)
                    return new NotFoundObjectResult($"No movie with this ID - {request.Id}");

                return new OkObjectResult(new MovieResponse(dbMovie));
            }
        }
    }
}
