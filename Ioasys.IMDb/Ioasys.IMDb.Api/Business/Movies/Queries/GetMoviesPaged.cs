﻿using Ioasys.IMDb.Api.Business.Movies.DTOs;
using Ioasys.IMDb.Infrastructure.Repositories.MovieRepository;
using Ioasys.IMDb.Infrastructure.Utils;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Api.Business.Movies.Queries
{
    public class GetMoviesPaged
    {
        public class Contract : IRequest<IActionResult>
        {
            public string NameFilter { get; set; }
            public int PageNumber { get; set; }
            public int PageSize { get; set; }
        }

        public class Handler : IRequestHandler<Contract, IActionResult>
        {
            private readonly IMovieRepository movieRepository;

            public Handler(IMovieRepository movieRepository)
            {
                this.movieRepository = movieRepository;
            }

            public async Task<IActionResult> Handle(Contract request, CancellationToken cancellationToken)
            {
                var dbMovies = await movieRepository.GetAllPaged(request.NameFilter, request.PageSize, request.PageNumber);

                var responseMovies = new PagedList<MovieResponse>(dbMovies.Page.Select(x => new MovieResponse(x)).ToList(), dbMovies.Count, dbMovies.PageSize, dbMovies.CurrentPage);

                return new OkObjectResult(responseMovies);
            }
        }
    }
}
