﻿using Ioasys.IMDb.Domain.Aggregates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Api.Business.Movies.DTOs
{
    public class MovieResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public int CountVotes { get; set; }

        public MovieResponse(Movie movie)
        {
            Id = movie.Id;
            Name = movie.Name;
            Rate = movie.Rate;
            CountVotes = movie.CountVotes;
        }
    }
}
