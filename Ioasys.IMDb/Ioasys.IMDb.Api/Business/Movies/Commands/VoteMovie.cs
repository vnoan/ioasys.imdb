﻿using Ioasys.IMDb.Api.Business.Movies.DTOs;
using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Infrastructure.Repositories.MovieRepository;
using Ioasys.IMDb.Infrastructure.Repositories.VoteRepository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Api.Business.Movies.Commands
{
    public class VoteMovie
    {
        public class Contract : IRequest<IActionResult>
        {
            public int MovieId { get; set; }
            public int UserId { get; set; }
            public decimal Rate { get; set; }
        }

        public class Handler : IRequestHandler<Contract, IActionResult>
        {
            private readonly IMovieRepository movieRepository;
            private readonly IVoteRepository voteRepository;

            public Handler(IMovieRepository movieRepository, IVoteRepository voteRepository)
            {
                this.movieRepository = movieRepository;
                this.voteRepository = voteRepository;
            }

            public async Task<IActionResult> Handle(Contract request, CancellationToken cancellationToken)
            {
                try
                {
                    var vote = await voteRepository.GetByUserAndMovie(request.UserId, request.MovieId, cancellationToken);

                    if(vote == null)
                    {
                        await NewVote(request, cancellationToken);
                    }
                    else
                    {
                        await UpdateVote(vote, request, cancellationToken);
                    }

                    return new OkResult();
                }
                catch(Exception e)
                {
                    return new BadRequestObjectResult(e.Message);
                }
            }

            private async Task NewVote(Contract request, CancellationToken cancellationToken)
            {
                var newVote = new Vote(request.UserId, request.MovieId, request.Rate);
                var movie = await movieRepository.Get(request.MovieId, cancellationToken);
                movie.AddVote(newVote);
                await movieRepository.Update(movie, cancellationToken);

            }

            private async Task UpdateVote(Vote vote, Contract request, CancellationToken cancellationToken)
            {
                var movie = vote.Movie;
                movie.RemoveVote(vote);
                vote.UpdateRate(request.Rate);
                movie.AddVote(vote);
                await movieRepository.Update(movie, cancellationToken);
            }
        }
    }
}
