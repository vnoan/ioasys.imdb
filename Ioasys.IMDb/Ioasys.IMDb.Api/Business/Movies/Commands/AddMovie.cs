﻿using Ioasys.IMDb.Api.Business.Movies.DTOs;
using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Infrastructure.Repositories.MovieRepository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Api.Business.Movies.Commands
{
    public class AddMovie
    {
        public class Contract : IRequest<IActionResult>
        {
            public string Name { get; set; }
        }

        public class Handler : IRequestHandler<Contract, IActionResult>
        {
            private readonly IMovieRepository movieRepository;

            public Handler(IMovieRepository movieRepository)
            {
                this.movieRepository = movieRepository;
            }

            public async Task<IActionResult> Handle(Contract request, CancellationToken cancellationToken)
            {
                var dbMovie = await movieRepository.Get(request.Name, cancellationToken);

                if (dbMovie != null)
                    return new BadRequestObjectResult($"Movie with this name already exists - {request.Name}");

                var movie = new Movie(request.Name);
                var newDbMovie = await movieRepository.Add(movie, cancellationToken);
                return new OkObjectResult(new MovieResponse(newDbMovie));
            }
        }
    } 
}
