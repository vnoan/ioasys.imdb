﻿using Ioasys.IMDb.Api.Business.Movies.DTOs;
using Ioasys.IMDb.Domain.Aggregates;
using Ioasys.IMDb.Infrastructure.Repositories.MovieRepository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Api.Business.Movies.Commands
{
    public class RemoveMovie
    {
        public class Contract : IRequest<IActionResult>
        {
            public int Id { get; set; }
        }

        public class Handler : IRequestHandler<Contract, IActionResult>
        {
            private readonly IMovieRepository movieRepository;

            public Handler(IMovieRepository movieRepository)
            {
                this.movieRepository = movieRepository;
            }

            public async Task<IActionResult> Handle(Contract request, CancellationToken cancellationToken)
            {
                var dbMovie = await movieRepository.Get(request.Id, cancellationToken);

                if (dbMovie == null)
                    return new BadRequestObjectResult($"No movie found with this ID - {request.Id}");

                await movieRepository.Delete(dbMovie, cancellationToken);
                return new OkResult();

            }
        }
    } 
}
