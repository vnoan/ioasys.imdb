﻿using Ioasys.IMDb.Api.Business.Movies.DTOs;
using Ioasys.IMDb.Api.Services;
using Ioasys.IMDb.Infrastructure.Repositories.MovieRepository;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Api.Business.Auth.Queries
{
    public class Login
    {
        public class Contract : IRequest<IActionResult>
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public class LoginResponse
        {
            public string Username { get; set; }
            public string Token { get; set; }
        }

        public class Handler : IRequestHandler<Contract, IActionResult>
        {
            private readonly AuthService authService;
            private readonly IUserRepository userRepository;

            public Handler(AuthService authService, IUserRepository userRepository)
            {
                this.authService = authService;
                this.userRepository = userRepository;
            }

            public async Task<IActionResult> Handle(Contract request, CancellationToken cancellationToken)
            {
                var user = await  userRepository.Get(request.Username, cancellationToken);
                if (user == null)
                    return new NotFoundObjectResult($"User not foud - {request.Username}");

                var isCorrect = authService.MatchPassword(user, request.Password);
                if (!isCorrect)
                    return new BadRequestObjectResult($"Username or password invalid");

                var token = authService.GenerateToken(user);
                var response = new LoginResponse
                {
                    Token = token,
                    Username = request.Username
                };

                return new OkObjectResult(response);
            }
        }
    }
}
