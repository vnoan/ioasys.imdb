using Ioasys.IMDb.Infrastructure;
using Ioasys.IMDb.Infrastructure.Repositories.MovieRepository;
using Ioasys.IMDb.Infrastructure.Repositories.VoteRepository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using MediatR;
using Ioasys.IMDb.Api.Services;

namespace Ioasys.IMDb.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Ioasys.IMDb.Api", Version = "v1" });
            });

            AddDbServices(services);

            AddAuthenticationConfig(services);
            
            services.AddMediatR(typeof(Startup));

            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IVoteRepository, VoteRepository>();
            services.AddScoped<IUserRepository, UserRepository>(); 
            services.AddSingleton<AuthService>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Ioasys.IMDb.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            RunMigration(app, env);

        }

        private void AddDbServices(IServiceCollection services)
        {
            var connString = Configuration.GetConnectionString("DefaultConnection");
            var migrationAssembly = typeof(DomainDbContext).GetTypeInfo().Assembly.GetName().Name;
            services.AddDbContext<DomainDbContext>(
                o => o.UseMySql(connString, new MySqlServerVersion(new Version(5, 7)),
                    x => x.MigrationsAssembly(migrationAssembly).MigrationsHistoryTable("__MigrationsHistory")));
           
        }

        private void RunMigration(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                if (!env.IsDevelopment())
                    return;
                serviceScope.ServiceProvider.GetService<DomainDbContext>().Database.Migrate();
            }
        }

        private void AddAuthenticationConfig(IServiceCollection services)
        {
            var sect = Configuration.GetSection("JWT");
            var key = Encoding.ASCII.GetBytes(sect["Secret"]);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    RequireExpirationTime = true
                };
            });

        }
    }
}
