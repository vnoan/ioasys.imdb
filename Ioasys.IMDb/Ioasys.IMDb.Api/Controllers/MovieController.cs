﻿using Ioasys.IMDb.Api.Business.Movies.Commands;
using Ioasys.IMDb.Api.Business.Movies.Queries;
using Ioasys.IMDb.Domain.Aggregates;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class MovieController : ControllerBase
    {
        private readonly ILogger<MovieController> _logger;
        private readonly IMediator _mediator;

        public MovieController(ILogger<MovieController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet("getMovie")]
        public async Task<IActionResult> GetMovie([FromQuery] GetMovie.Contract request)
        {
            var response = await _mediator.Send(request);
            return response;
        }

        [HttpGet("getPaged")]
        public async Task<IActionResult> GetPaged([FromQuery] GetMoviesPaged.Contract request)
        {
            var response = await _mediator.Send(request);
            return response;
        }

        [HttpPost("addMovie")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> AddMovie([FromBody] AddMovie.Contract request)
        {
            var response = await _mediator.Send(request);
            return response;
        }

        [HttpPut("updateMovie")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateMovie([FromBody] UpdateMovie.Contract request)
        {
            var response = await _mediator.Send(request);
            return response;
        }


        [HttpDelete("removeMovie")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> RemoveMovie([FromQuery] RemoveMovie.Contract request)
        {
            var response = await _mediator.Send(request);
            return response;
        }


        [HttpPost("voteMovie")]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> VoteMovie([FromBody] VoteMovie.Contract request)
        {
            var response = await _mediator.Send(request);
            return response;
        }



    }
}
