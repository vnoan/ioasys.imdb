﻿using Ioasys.IMDb.Api.Business.Auth.Queries;
using Ioasys.IMDb.Api.Business.Movies.Commands;
using Ioasys.IMDb.Api.Business.Movies.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.IMDb.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<MovieController> _logger;
        private readonly IMediator _mediator;

        public AuthController(ILogger<MovieController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> GetMovie([FromBody] Login.Contract request)
        {
            var response = await _mediator.Send(request);
            return response;
        }


    }
}
